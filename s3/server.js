const express = require('express');
const bodyParser = require('body-parser');

const app = express();
app.use(bodyParser.json());

let products = [
    {
        id: 0,
        productName: 'Samsung Galaxy S9',
        price: 4000
    },
    {
        id: 1,
        productName: 'Iphone XR',
        price: 3999
    }
];

app.get('/get-all', (req, res) => {
    res.status(200).send(products);
})

app.post('/add', (req, res) => {
    if (req.body.productName && req.body.price) {
        let product = {
            id: products.length,
            productName: req.body.productName,
            price: req.body.price
        };
        products.push(product);
        res.status(200).send(product);
    } else {
        res.status(500).send('Error!');
    }
});

app.put('/update:id', async (req, res) => {
    try {
        let product = await products.findById(req.params.id)
        if (product) {
            await product.update(req.body);
            res.status(200).send("Modified successfully");
        }
        else {
            res.status(404).send("Modified successfully");
        }
    }
    catch (error) {
        res.status(500).send(error);
    }
});

app.delete('/delete', (req, res) => {
    const denumireProdus = req.body.productName;
    products.destroy(req.body, {
        where: { denumireProdus: denumireProdus }

    }).then(() => {
        res.status(200).send("Deleted successfully");
    }).catch(err => {
        res.status(500).send(err);
    });
});

app.listen(8080, () => {
    console.log('Server started on port 8080...');
});