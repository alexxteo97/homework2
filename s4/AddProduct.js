import React from 'react';

export class AddProduct extends React.Component {
    
    constructor(props){
        super(props);
        this.state = {
            id: 0,
            productName: "",
            price: 0
        };
    }
    
    clearFields = () => {
        this.setState({
            id: 0,
            productName: " ",
            price: 0
        });
    }
    
    handleChangeId = (e) => {
        this.setState({
           id: e.target.value
        });
        
    }
    
    handleChangeProductName = (e) => {
        this.setState({
            productName: e.target.value
        });
        
    }
    
    handleChangePrice = (e) => {
        this.setState({
            price: e.target.value
        });
        
    }
    
    onItemAdded = () => {
      let todo = {
          id: this.state.id,
          productName: this.state.productName,
          price: this.state.price
      };
      this.props.handleAdd(todo);
      this.clearFields();
    }
    
    render(){
        return (
            <React.Fragment>
                <div>
                    <h1>Add product</h1>
                    <div>
                        <input type="number" placeholder="Id" value={this.state.id} onChange={this.handleChangeId} />
                        <input type="text" placeholder="ProductName" value={this.state.productName} onChange={this.handleChangeProductName}/>
                        <input type="number" value={this.state.price}  onChange={this.handleChangePrice}/>
                        <button onClick={this.onItemAdded}>Add product</button>
                    </div>
                </div>
            </React.Fragment>
            );
    }
}
